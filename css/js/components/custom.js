$(function () {
    if (!$("#headerlogin").length) {
        $('.topbar-menu-right').find('#navbar-login').remove();
    }
    if (!$("#headerlogout").length) {
        $('.topbar-menu-right').find('#navbar-logout').remove();
    }
    $('#pagecontent').find('.visual').insertAfter($('.navbar')); // set visual to visual content in header

    if($('#body_design')) {
        var backLink = $('.crumb-mobile');

        $('#crumbs').append(backLink);
    }

    $('#crumbs a[href="/"]').html('<span class="crumb-home"></i>');

    if($('#sidebar')) {
        $("nav #filter_cards").prepend('<h3>Filters</h3><a class="filter-w" href="javascript:;">' +
            '<i class="fa fa-refresh"></i>Filters wissen</a>');
    }

    if($(window).width() < 991) {
        $('#navbar-basket').text(function(index, text){
            return text.replace('items', '');
        });
    }


});
