$(function() {

    var readMore = $('.js-component-read-more');

    readMore.each(function(){
        var that = this;
        var readMoreButton = $(this).find('.read-more-link');

        readMoreButton.on('click',function(){
            $(that).toggleClass('open');
            if ($(readMoreButton).html() != 'minder') {
               $(readMoreButton).html('minder'); 
            } else {
                $(readMoreButton).html('lees meer'); 
            }
        })
    });

});
